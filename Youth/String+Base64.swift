//
//  String+Base64.swift
//  Youth
//
//  Created by fiveze on 25.07.15.
//  Copyright (c) 2015 Sergey Tursunov. All rights reserved.
//

extension String {
    func base64Encode() -> String {
        let plainData = (self as NSString).dataUsingEncoding(NSUTF8StringEncoding)!
        let base64EncodedString = plainData.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
        
        return base64EncodedString
    }
}