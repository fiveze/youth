//
//  User.swift
//  Youth
//
//  Created by fiveze on 25.07.15.
//  Copyright (c) 2015 Sergey Tursunov. All rights reserved.
//

import UIKit

class User: NSObject {
    
    static func registerUserFromFB(accessToken: String, userId: String, expirationDate: NSDate) {
        let (username, password) = generateLoginAndPasswordEncodedBase64()
        
        var user = PFUser()
        user.username = username
        user.password = password
        user["authData"] = ["facebook": ["id": userId, "access_token": accessToken, "expiration_date": expirationDate]]
        
        user.signUpInBackgroundWithBlock { (success: Bool, error: NSError?) -> Void in
            if error == nil {
                println("user created")
            } else {
                println("Error: \(error)")
            }
        }
    }
    
    static func registerUserFromVK(accessToken: String, userId: String, expirationDate: String) {
        let (username, password) = generateLoginAndPasswordEncodedBase64()

        var user = PFUser()
        user.username = username
        user.password = password
        user["vkAuthData"] = ["vkontakte": ["id": userId, "access_token": accessToken, "expiration_date": expirationDate]]
        
        user.signUpInBackgroundWithBlock { (success: Bool, error: NSError?) -> Void in
            if error == nil {
                println("user created")
            } else {
                println("Error: \(error)")
            }
        }
    }
    
    static private func generateLoginAndPasswordEncodedBase64() -> (username: String, password: String) {
        var username = ""
        var password = ""
        
        for _ in 0...24 {
            let randomCharacterForUsername = UnicodeScalar(arc4random_uniform(255))
            let randomCharacterForPassword = UnicodeScalar(arc4random_uniform(255))
            
            username.append(randomCharacterForUsername)
            password.append(randomCharacterForPassword)
        }
        
        return (username.base64Encode(), password.base64Encode())
    }
}