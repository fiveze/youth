//
//  Place.swift
//  Youth
//
//  Created by fiveze on 02.08.15.
//  Copyright (c) 2015 Sergey Tursunov. All rights reserved.
//

import UIKit

class Place: NSObject {
    struct Location {
        var address: String?
        var distance: Int
        var latitude: Double
        var longtitude: Double
    }
    
    var name: String!
    var location: Location!

    init(place: NSDictionary) {
        self.name = place.objectForKey("name") as! String
        
        let location = place.objectForKey("location") as! NSDictionary
        let distance = location.objectForKey("distance") as! Int
        let latitude = location.objectForKey("lat") as! Double
        let longtitude = location.objectForKey("lng") as! Double
        
        var address = location.objectForKey("address") as? String
        
        if address == nil {
            address = (location.objectForKey("formattedAddress") as? [String])?.first
        }
        
        self.location = Location(address: address, distance: distance, latitude: latitude, longtitude: longtitude)
    }
    
    static func getPlacesNear(longtitude: Double, latitude: Double, query: String?, completion: ((places: [Place]) -> ())?) {
        let manager = AFHTTPRequestOperationManager()
        let ll = String(stringInterpolationSegment: longtitude) + "," + String(stringInterpolationSegment: latitude)
        
        var params = ["ll": ll]
        
        if let query = query {
            params["query"] = query
        }
        
        let url = getUrlApi("venues/search", params: params)
        
        manager.GET(url, parameters: nil, success: { (operation: AFHTTPRequestOperation!, response: AnyObject!) -> Void in
            if let venues = response.objectForKey("response")?.objectForKey("venues") as? [NSDictionary] {
                var places = [Place]()
                
                for venue in venues {
                    places.append(Place(place: venue))
                }
                
                if let completion = completion {
                    completion(places: places)
                }
            }
            }) { (operation: AFHTTPRequestOperation!, error: NSError!) -> Void in
            println("error")
        }
    }
    
    static func getUrlApi(method: String, var params: Dictionary<String, String>) -> String {
        let apiDomain = "https://api.foursquare.com/v2/"
        let clientId = "240WLADCYF4ZWKTEJV4LUSDUEXUIAHAOGH33FXHTCIXSBUIQ"
        let clientSecret = "LTN5XWDWHL2BRP0XJUVB2JAGWQONG4RFTN2OCRDXHPNNA5OW"
        let version = "20130815"
        
        var urlApi = apiDomain + method + "/?"
        params["client_id"] = clientId
        params["client_secret"] = clientSecret
        params["v"] = version
        
        for param in params {
            let urlParam = "&" + param.0 + "=" + param.1.stringByAddingPercentEncodingWithAllowedCharacters(.URLHostAllowedCharacterSet())!
            urlApi += urlParam
        }
        
        return urlApi
    }
}
