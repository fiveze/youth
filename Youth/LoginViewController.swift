//
//  LoginViewController.swift
//  Youth
//
//  Created by fiveze on 23.07.15.
//  Copyright (c) 2015 Sergey Tursunov. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, VKSdkDelegate, FBSDKLoginButtonDelegate {
    
    @IBOutlet weak var vkLoginButton: VKLoginButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        VKSdk.initializeWithDelegate(self, andAppId: "5007978")
        
        if VKSdk.wakeUpSession() {
            println("ok")
        } else {
            println("error")
        }
        
        let recognizer = UITapGestureRecognizer(target: self, action: "vkLoginButtonClicked:")
        recognizer.numberOfTapsRequired = 1
        recognizer.numberOfTouchesRequired = 1
        vkLoginButton.addGestureRecognizer(recognizer)
        vkLoginButton.userInteractionEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func vkLoginButtonClicked(recognizer: UIGestureRecognizer) {
        VKSdk.authorize(["friends", "offline"], revokeAccess: true, forceOAuth: false, inApp: false)
    }
    
    // MARK: – FBSDKLoginButtonDelegate methods
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        println(result.token)
        
        if !result.isCancelled {
            let accessToken = result.token.tokenString
            let userId = result.token.userID
            let expirationDate = result.token.expirationDate
            
            User.registerUserFromFB(accessToken, userId: userId, expirationDate: expirationDate)
        } else {
            println("facebook auth is cancelled")
        }
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        
    }
    
    // MARK: – VKSdkDelegate methods
    
    func vkSdkNeedCaptchaEnter(captchaError: VKError!) {
        let vc = VKCaptchaViewController.captchaControllerWithError(captchaError)
        vc.presentIn(self.navigationController?.topViewController)
    }
    
    func vkSdkTokenHasExpired(expiredToken: VKAccessToken!) {
        println("vkSdkTokenHasExpired")
    }

    func vkSdkUserDeniedAccess(authorizationError: VKError!) {
        println("vkSdkUserDeniedAccess")
    }
    
    func vkSdkShouldPresentViewController(controller: UIViewController!) {
        self.navigationController?.topViewController.presentViewController(controller, animated: true, completion: nil)
    }
    
    func vkSdkReceivedNewToken(newToken: VKAccessToken!) {
        let accessToken = newToken.accessToken
        let userId = newToken.userId
        let expirationDate = newToken.expiresIn
        
        User.registerUserFromVK(accessToken, userId: userId, expirationDate: expirationDate)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
